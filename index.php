<?php
	include 'config.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<meta name="description" content="Viajes" />
	<meta name="keywords" content="" />
	<title>Alianza Tour</title>
	<link rel="icon" href="imagenes/icono.png" />
	<link rel="image_src" href="imagenes/logo.png" />
	<link rel="stylesheet" href="css/normalize.css" />
	<link rel="stylesheet" href="css/iconos/style.css" />
	<link rel="stylesheet" href="css/default/default.css" />
	<link rel="stylesheet" href="css/nivo_slider.css" />
	<link rel="stylesheet" href="css/owl_carousel.css" />
	<link rel="stylesheet" href="css/owl_theme_min.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/inicio.css" />
	<script src="js/jquery_2_1_1.js"></script>
	<script src="js/owl_carousel_min.js"></script>
	<script src="js/scripag.js"></script>
</head>
<body>
	<!--figure id="inic">
		<img src="imagenes/home_inicio.jpg" alt="home" />
	</figure-->
	<article id="inic">
		<iframe id="ifrtick" src="https://www.e-agencias.com.co/alianzatourcucuta" 
			seamless="seamless" frameborder="0" scrolling="auto">
		</iframe>
	</article>
	<header>
		<figure id="logo">
			<a href="">
				<img src="imagenes/logob.png" alt="logo" />
			</a>
		</figure>
		<div id="mn_mv"><span class="icon-menu"></span></div>
		<nav id="mnP">
			<ul>
				<li><a class="sull" href="">Inicio</a></li>
				<li><a href="nosotros">Nosotros</a></li>
				<li class="submen" data-num="1">
					<a href="planes">Planes</a>
					<ul class="children1">
						<?php
							$tpPL="SELECT * from tip_planes order by id_tppl desc";
							$sql_pl=mysql_query($tpPL,$conexion) or die (mysql_error());
							while ($pl=mysql_fetch_array($sql_pl)) {
								$idlp=$pl['id_tppl'];
								$nmlp=$pl['nm_tppl'];
						?>
						<li><a href="planes/tipos.php?tp=<?php echo $idlp ?>"><?php echo "$nmlp"; ?></a></li>
						<?php
							}
						?>
					</ul>
				</li>
				<li class="submen" data-num="2"><a href="promociones">Promociones</a>
					<ul class="children2">
						<?php
							$tpPR="SELECT * from tip_pr order by id_tpr desc";
							$sql_pr=mysql_query($tpPR,$conexion) or die (mysql_error());
							while ($pr=mysql_fetch_array($sql_pr)) {
								$idpr=$pr['id_tpr'];
								$nmpr=$pr['nm_tpr'];
						?>
						<li><a href="promociones/tipos.php?tp=<?php echo $idpr ?>"><?php echo "$nmpr"; ?></a></li>
						<?php
							}
						?>
					</ul>
				</li>
				<li><a href="documento">Documentos</a></li>
				<li><a href="contacto">Contacto</a></li>
			</ul>
		</nav>
		<div id="usuario">
			<span class="icon-login"></span>
		</div>
	</header>
	<section>
		<figure class="otrosS">
			<div class="slider-wrapper theme-default">
				<div id="sliderBB" class="nivoSlider">
					<?php
						$redils="SELECT * from slider_tx order by id_slix desc";
						$sql_ils=mysql_query($redils,$conexion) or die (mysql_error());
						while ($is=mysql_fetch_array($sql_ils)) {
							$idsl=$is['id_slix'];
							$ttsl=$is['tt_sli'];
							$sbsl=$is['sb_sli'];
							$resl=$is['re_sli'];
							$lksl=$is['lk_sli'];
							$rtsl=$is['rut_sli'];
					?>
					<img src="<?php echo $rtsl ?>" alt="<?php echo $ttsl ?>" />
					<?php
						}
					?>
				</div>
			</div>
		</figure>
		<!--article class="owl-carousel owl-theme owl-loaded">
			<?php
				$redils="SELECT * from slider_tx order by id_slix desc";
				$sql_ils=mysql_query($redils,$conexion) or die (mysql_error());
				while ($is=mysql_fetch_array($sql_ils)) {
					$idsl=$is['id_slix'];
					$ttsl=$is['tt_sli'];
					$sbsl=$is['sb_sli'];
					$resl=$is['re_sli'];
					$lksl=$is['lk_sli'];
					$rtsl=$is['rut_sli'];
			?>
			<div class="item">
				<article class="fondcontx" style="background-image:url(<?php echo $rtsl ?>)">
					<article>
						<hgroup>
							<h2><?php echo "$ttsl"; ?></h2>
							<?php
								if ($sbsl!="") {
							?>
							<h3><?php echo "$sbsl"; ?></h3>
							<?php
								}
							?>
						</hgroup>
						<div id="mindv"></div>
						<p>
							<?php echo "$resl"; ?>
						</p>
						<?php
							if ($lksl!="") {
						?>
						<a href="<?php echo $lksl ?>">Ver todo</a>
						<?php
							}
						?>
					</article>
				</article>
			</div>
			<?php
				}
			?>
		</article-->
		<h2 id="hdos">PLANES RECIENTES</h2>
		<article id="automargen">
			<article class="planflx">
				<?php
					$rcpl="SELECT * from planes order by id_pl desc limit 6";
					$sql_rc=mysql_query($rcpl,$conexion) or die (mysql_error());
					while ($cr=mysql_fetch_array($sql_rc)) {
						$idcr=$cr['id_pl'];
						$tpcr=$cr['tp_pl'];
						$nmcr=$cr['tit_pl'];
						$sbcr=$cr['sub_pl'];
						$xxcr=$cr['txt_pl'];
						$escr=$cr['es_pl'];
						$fecr=$cr['fe_pl'];
						$fpcr=$cr['rut_pdf'];
						$primerimg="SELECT * from images_planes where pl_id=$idcr order by id_img_pl asc limit 1";
						$sql_img=mysql_query($primerimg,$conexion) or die (mysql_error());
						$numimg=mysql_num_rows($sql_img);
						if ($numimg>0) {
							while ($tr=mysql_fetch_array($sql_img)) {
								$idmg=$tr['id_img_pl'];
								$rtmg=$tr['rut_pl'];
							}
						}
						else{
							$idmg=0;
							$rtmg="imagenes/predeterminado.png";
						}
						//planes/desc.php?pl=<?php echo $idcr
				?>
				<figure>
					<a href="<?php echo $fpcr ?>">
						<img src="<?php echo $rtmg ?>" alt="<?php echo $idcr ?>" />
					</a>
					<figcaption>
						<h3><?php echo "$nmcr"; ?></h3>
						<p><?php echo "$sbcr"; ?></p>
						<div id="minln"></div>
					</figcaption>
				</figure>
				<?php
					}
				?>
			</article>
		</article>
		<h2 id="hdos">CRUCEROS</h2>
		<article id="automargen">
			<figure id="galcrus">
				<div class="slider-wrapper theme-default">
					<div id="slider" class="nivoSlider">
						<?php
							$cucru="SELECT * from cruceros order by id_cu desc";
							$sql_cru=mysql_query($cucru,$conexion) or die (mysql_error());
							while ($urc=mysql_fetch_array($sql_cru)) {
								$idcu=$urc['id_cu'];
								$rtcu=$urc['rut_cu'];
						?>
						<img src="<?php echo $rtcu ?>" alt="<?php echo $idcu ?>" />
						<?php
							}
						?>
					</div>
				</div>
			</figure>
		</article>
		<article class="alianzz">
			<article id="automargen">
				<article class="owl-carousel-b owl-theme owl-loaded">
					<?php
						$alz="SELECT * from proveedores order by id_pv desc";
						$sql_alz=mysql_query($alz,$conexion) or die (mysql_error());
						while ($az=mysql_fetch_array($sql_alz)) {
							$idpv=$az['id_pv'];
							$ttpv=$az['tt_pv'];
							$lkpv=$az['lk_pv'];
							$rtpv=$az['rut_pv'];
					?>
					<div class="item">
						<figure>
							<a href="<?php echo $lkpv ?>" target="_blank">
								<img src="<?php echo $rtpv ?>" alt="<?php echo $ttpv ?>" />
							</a>
						</figure>
					</div>
					<?php
						}
					?>
				</article>
			</article>
		</article>
	</section>
	<section>
		<article id="map_canvas" class="mapas"></article>
	</section>
	<footer>
		<article class="foorflx">
			<article class="ar arr2">
				<h2>Alianza Tour </h2>
				<p>
					Brindar el mejor servicio de asesoría y venta de paquetes turísticos a 
					nivel local, nacional e internacional, con el fin de lograr la plena 
					satisfacción del cliente, brindando servicios que superen sus expectativas 
					mediante un servicio confiable, accesible y respaldado por un grupo de 
					trabajo profesional, incentivado por el buen trato y el impulso al 
					desarrollo individual de cada uno de los integrantes nuestra organización.
				</p>
			</article>
			<article class="ar">
				<h2>Contáctanos</h2>
				<div>Av 9 #5-53 Barrio panamericano</div>
				<div>Cúcuta Colombia</div>
				<div>Whatsapp: 311 806 8590</div>
				<div>Tel: 572 5613 - 572 4592 - 317 376 4207</div>
				<div>Email: ventas@alianzatourcucuta.com</div>
			</article>
			<article class="ar">
				<article id="redes">
					<a href="https://www.facebook.com/alianzatourcucuta/?fref=ts" target="_blank"><span class="icon-facebook4"></span></a>
					<a href="https://www.instagram.com/alianzatour/" target="_blank"><span class="icon-instagram2"></span></a>
				</article>
			</article>
		</article>
		<article class="footfin">
			CONAXPORT © 2015 &nbsp;&nbsp;todos los derechos reservados &nbsp;- &nbsp;PBX (5) 841 733 &nbsp;&nbsp;Cúcuta - Colombia &nbsp;&nbsp;
			<a href="http://conaxport.com/" target="_blank">www.conaxport.com</a>
		</article>
	</footer>
	<script src="js/nivo_slider.js"></script>
	<script src="js/sliimages.js"></script>
	<script src="js/sliddiv.js"></script>
	<script src="http://www.google.com/jsapi"></script>
	<script src="js/colmapa.js"></script>
</body>
</html>