-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-12-2015 a las 20:20:28
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `alianza`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE IF NOT EXISTS `administrador` (
  `id_adm` int(11) NOT NULL,
  `user_adm` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `cor_adm` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `pass_adm` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `tp_adm` varchar(10) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_adm`, `user_adm`, `cor_adm`, `pass_adm`, `tp_adm`) VALUES
(1, 'admin', 'admin@dominio.com', '136e494faa19a3eb37309f0bf079fab15f975e0c', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos_dc`
--

CREATE TABLE IF NOT EXISTS `archivos_dc` (
  `id_ar` int(11) NOT NULL,
  `tp_ar` int(11) NOT NULL,
  `nm_ar` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `rut_ar` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  `fe_ar` date NOT NULL,
  `es_ar` varchar(10) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruceros`
--

CREATE TABLE IF NOT EXISTS `cruceros` (
  `id_cu` int(11) NOT NULL,
  `tt_cu` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `rut_cu` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  `lk_cu` varchar(400) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cruceros`
--

INSERT INTO `cruceros` (`id_cu`, `tt_cu`, `rut_cu`, `lk_cu`) VALUES
(2, '', 'imagenes/cruseros/cru2.jpg', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento`
--

CREATE TABLE IF NOT EXISTS `documento` (
  `id_dc` int(11) NOT NULL,
  `tt_mn` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `tt_dc` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `txt_dc` text COLLATE utf8_spanish_ci NOT NULL,
  `fe_dc` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `documento`
--

INSERT INTO `documento` (`id_dc`, `tt_mn`, `tt_dc`, `txt_dc`, `fe_dc`) VALUES
(1, '', 'document1', '<p>te<span style="color:#DAA520">xto</span></p>\r\n', '2015-11-18'),
(2, 'menu1', 'tt', '<p>asdasd</p>\r\n', '2015-12-07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `images_planes`
--

CREATE TABLE IF NOT EXISTS `images_planes` (
  `id_img_pl` int(11) NOT NULL,
  `pl_id` int(11) NOT NULL,
  `rut_pl` varchar(455) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `images_planes`
--

INSERT INTO `images_planes` (`id_img_pl`, `pl_id`, `rut_pl`) VALUES
(5, 2, 'imagenes/planes/1200_800.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `images_promoc`
--

CREATE TABLE IF NOT EXISTS `images_promoc` (
  `id_img_pr` int(11) NOT NULL,
  `pr_id` int(11) NOT NULL,
  `rut_pr` varchar(455) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `images_promoc`
--

INSERT INTO `images_promoc` (`id_img_pr`, `pr_id`, `rut_pr`) VALUES
(3, 1, 'imagenes/promociones/1200_800_2.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planes`
--

CREATE TABLE IF NOT EXISTS `planes` (
  `id_pl` int(11) NOT NULL,
  `tp_pl` int(11) NOT NULL,
  `sub_pl` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `tit_pl` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `txt_pl` text COLLATE utf8_spanish_ci NOT NULL,
  `rut_pdf` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  `es_pl` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fe_pl` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `planes`
--

INSERT INTO `planes` (`id_pl`, `tp_pl`, `sub_pl`, `tit_pl`, `txt_pl`, `rut_pdf`, `es_pl`, `fe_pl`) VALUES
(2, 6, '', 'plan2', '', '', '1', '2015-11-17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promocion`
--

CREATE TABLE IF NOT EXISTS `promocion` (
  `id_pr` int(11) NOT NULL,
  `tp_pr` int(11) NOT NULL,
  `tit_pr` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `txt_pr` text COLLATE utf8_spanish_ci NOT NULL,
  `rut_pdf` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  `es_pr` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fe_pr` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `promocion`
--

INSERT INTO `promocion` (`id_pr`, `tp_pr`, `tit_pr`, `txt_pr`, `rut_pdf`, `es_pr`, `fe_pr`) VALUES
(1, 1, 'prom1', '', '', '1', '2015-11-17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE IF NOT EXISTS `proveedores` (
  `id_pv` int(11) NOT NULL,
  `tt_pv` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `rut_pv` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  `lk_pv` varchar(400) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_pv`, `tt_pv`, `rut_pv`, `lk_pv`) VALUES
(2, 'alianza', 'imagenes/alianzas/alb2.png', ''),
(3, 'alianza', 'imagenes/alianzas/ala2.png', ''),
(4, 'alianza', 'imagenes/alianzas/alc2.png', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider_tx`
--

CREATE TABLE IF NOT EXISTS `slider_tx` (
  `id_slix` int(11) NOT NULL,
  `tt_sli` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `sb_sli` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `re_sli` text COLLATE utf8_spanish_ci NOT NULL,
  `lk_sli` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  `rut_sli` varchar(455) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `slider_tx`
--

INSERT INTO `slider_tx` (`id_slix`, `tt_sli`, `sb_sli`, `re_sli`, `lk_sli`, `rut_sli`) VALUES
(4, 'titulo', 'subtitulo', 'caracteres de 14o  mÃ¡ximo', 'http://gitlab.com/albert925/', 'imagenes/galeria/home_inicio.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tip_planes`
--

CREATE TABLE IF NOT EXISTS `tip_planes` (
  `id_tppl` int(11) NOT NULL,
  `nm_tppl` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `rut_tppl` varchar(455) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tip_planes`
--

INSERT INTO `tip_planes` (`id_tppl`, `nm_tppl`, `rut_tppl`) VALUES
(3, 'tipo1', 'imagenes/tipos_planes/otro_1300.png'),
(6, 'tipo2', 'imagenes/tipos_planes/im1300_240.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tip_pr`
--

CREATE TABLE IF NOT EXISTS `tip_pr` (
  `id_tpr` int(11) NOT NULL,
  `nm_tpr` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tip_pr`
--

INSERT INTO `tip_pr` (`id_tpr`, `nm_tpr`) VALUES
(1, 'tipo1'),
(3, 'tipo3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tp_ar`
--

CREATE TABLE IF NOT EXISTS `tp_ar` (
  `id_tpar` int(11) NOT NULL,
  `doc_id` int(11) NOT NULL,
  `nm_tpar` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tp_ar`
--

INSERT INTO `tp_ar` (`id_tpar`, `doc_id`, `nm_tpar`) VALUES
(6, 2, 'tipo2');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id_adm`);

--
-- Indices de la tabla `archivos_dc`
--
ALTER TABLE `archivos_dc`
  ADD PRIMARY KEY (`id_ar`),
  ADD KEY `tipo_arch` (`tp_ar`);

--
-- Indices de la tabla `cruceros`
--
ALTER TABLE `cruceros`
  ADD PRIMARY KEY (`id_cu`);

--
-- Indices de la tabla `documento`
--
ALTER TABLE `documento`
  ADD PRIMARY KEY (`id_dc`);

--
-- Indices de la tabla `images_planes`
--
ALTER TABLE `images_planes`
  ADD PRIMARY KEY (`id_img_pl`),
  ADD KEY `img_pl` (`pl_id`);

--
-- Indices de la tabla `images_promoc`
--
ALTER TABLE `images_promoc`
  ADD PRIMARY KEY (`id_img_pr`),
  ADD KEY `images_pr` (`pr_id`);

--
-- Indices de la tabla `planes`
--
ALTER TABLE `planes`
  ADD PRIMARY KEY (`id_pl`),
  ADD KEY `tip_plan` (`tp_pl`);

--
-- Indices de la tabla `promocion`
--
ALTER TABLE `promocion`
  ADD PRIMARY KEY (`id_pr`),
  ADD KEY `tip_promo` (`tp_pr`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_pv`);

--
-- Indices de la tabla `slider_tx`
--
ALTER TABLE `slider_tx`
  ADD PRIMARY KEY (`id_slix`);

--
-- Indices de la tabla `tip_planes`
--
ALTER TABLE `tip_planes`
  ADD PRIMARY KEY (`id_tppl`);

--
-- Indices de la tabla `tip_pr`
--
ALTER TABLE `tip_pr`
  ADD PRIMARY KEY (`id_tpr`);

--
-- Indices de la tabla `tp_ar`
--
ALTER TABLE `tp_ar`
  ADD PRIMARY KEY (`id_tpar`),
  ADD KEY `doc_idtp` (`doc_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id_adm` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `archivos_dc`
--
ALTER TABLE `archivos_dc`
  MODIFY `id_ar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `cruceros`
--
ALTER TABLE `cruceros`
  MODIFY `id_cu` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `documento`
--
ALTER TABLE `documento`
  MODIFY `id_dc` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `images_planes`
--
ALTER TABLE `images_planes`
  MODIFY `id_img_pl` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `images_promoc`
--
ALTER TABLE `images_promoc`
  MODIFY `id_img_pr` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `planes`
--
ALTER TABLE `planes`
  MODIFY `id_pl` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `promocion`
--
ALTER TABLE `promocion`
  MODIFY `id_pr` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_pv` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `slider_tx`
--
ALTER TABLE `slider_tx`
  MODIFY `id_slix` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tip_planes`
--
ALTER TABLE `tip_planes`
  MODIFY `id_tppl` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tip_pr`
--
ALTER TABLE `tip_pr`
  MODIFY `id_tpr` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tp_ar`
--
ALTER TABLE `tp_ar`
  MODIFY `id_tpar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `archivos_dc`
--
ALTER TABLE `archivos_dc`
  ADD CONSTRAINT `archivos_dc_ibfk_1` FOREIGN KEY (`tp_ar`) REFERENCES `tp_ar` (`id_tpar`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `images_planes`
--
ALTER TABLE `images_planes`
  ADD CONSTRAINT `images_planes_ibfk_1` FOREIGN KEY (`pl_id`) REFERENCES `planes` (`id_pl`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `images_promoc`
--
ALTER TABLE `images_promoc`
  ADD CONSTRAINT `images_promoc_ibfk_1` FOREIGN KEY (`pr_id`) REFERENCES `promocion` (`id_pr`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `planes`
--
ALTER TABLE `planes`
  ADD CONSTRAINT `planes_ibfk_1` FOREIGN KEY (`tp_pl`) REFERENCES `tip_planes` (`id_tppl`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `promocion`
--
ALTER TABLE `promocion`
  ADD CONSTRAINT `promocion_ibfk_1` FOREIGN KEY (`tp_pr`) REFERENCES `tip_pr` (`id_tpr`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tp_ar`
--
ALTER TABLE `tp_ar`
  ADD CONSTRAINT `tp_ar_ibfk_1` FOREIGN KEY (`doc_id`) REFERENCES `documento` (`id_dc`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
