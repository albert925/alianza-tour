<?php
	include '../../config.php';
	session_start();
	if (isset($_SESSION['adm'])) {
		$seR=$_SESSION['adm'];
		$datad="SELECT * from administrador where id_adm=$seR";
		$sqlad=mysql_query($datad,$conexion) or die (mysql_error());
		while ($ad=mysql_fetch_array($sqlad)) {
			$idad=$ad['id_adm'];
			$usad=$ad['user_adm'];
			$tpad=$ad['tp_adm'];
		}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, maximun-scale=1" />
	<meta name="description" content="Datos <?php echo $usad ?>" />
	<meta name="keywords" content="admin" />
	<title><?php echo "$usad"; ?> | Alianza Tour</title>
	<link rel="icon" href="../../imagenes/icono.png" />
	<link rel="image_src" href="../../imagenes/logo.png" />
	<link rel="stylesheet" href="../../css/normalize.css" />
	<link rel="stylesheet" href="../../css/iconos/style.css" />
	<link rel="stylesheet" href="../../css/style.css" />
	<link rel="stylesheet" href="../../css/sty_adm.css" />
	<script src="../../js/jquery_2_1_1.js"></script>
	<script src="../../js/scripag.js"></script>
	<script src="../../js/admin.js"></script>
</head>
<body>
	<header>
		<figure id="logo">
			<a href="">
				<img src="../../imagenes/logob.png" alt="logo" />
			</a>
		</figure>
		<div id="mn_mv"><span class="icon-menu"></span></div>
		<nav id="mnP">
			<ul>
				<li class="submen" data-num="1">
					<a href="slider">Slider I.</a>
					<ul class="children1">
						<li><a href="proveedor">Proveedores</a></li>
						<li><a href="cruceros">Cruceros</a></li>
					</ul>
				</li>
				<li><a href="planes">Planes</a></li>
				<li><a href="promocion">Promociones</a></li>
				<li class="submen" data-num="2"><a href="documento">Documentos</a>
					<ul class="children2">
						<li><a href="archivos">Archivos</a></li>
					</ul>
				</li>
				<li><a class="sull" href=""><?php echo "$usad"; ?></a></li>
				<li><a href="../../cerrar">Salir</a></li>
			</ul>
		</nav>
	</header>
	<section>
		<h1>Admnistrador <?php echo "$usad"; ?></h1>
		<article id="automargen" class="fla">
			<article class="columninput">
				<h2>Usuario</h2>
				<input type="text" id="fus" value="<?php echo $usad ?>" />
				<div id="txA"></div>
				<input type="submit" value="Cambiar" data-id="<?php echo $idad ?>" id="camA" />
			</article>
			<article class="cirT">
				<div id="circ">
					<?php echo substr($usad,0,1); ?>
				</div>
			</article>
			<article class="columninput">
				<h2>Contraseña</h2>
				<label>*<b>Contraseña actual</b></label>
				<input type="password" id="cac" requried />
				<label>*<b>Contraseña nueva</b></label>
				<input type="password" id="cna" required />
				<label>*<b>Repite la contraseña nueva</b></label>
				<input type="password" id="cnb" required />
				<div id="txB"></div>
				<input type="submit" value="Cambiar" data-id="<?php echo $idad ?>" id="camB" />
			</article>
		</article>
	</section>
	<footer>
		<article class="foorflx">
			<article class="ar arr2">
				<h2>Alianza Tour </h2>
				<p>
					Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.
				</p>
				<p>
					Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de "Lorem Ipsum" va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo.
				</p>
			</article>
			<article class="ar">
				<h2>Contáctanos</h2>
				<div>Av 0 N 11-89 Local 105 CAOBOS</div>
				<div>Cúcuta Colombia</div>
				<div>Horario: Lunes-Viernes 9am-6:00pm</div>
				<div>Tel: 5 72 3840 - 324 567 2981</div>
				<div>Email: contacto@alianzatour.com</div>
			</article>
			<article class="ar">
				<article id="redes">
					<a href="" target="_blank"><span class="icon-facebook4"></span></a>
					<a href="" target="_blank"><span class="icon-instagram2"></span></a>
					<a href="" target="_blank"><span class="icon-youtube5"></span></a>
					<a href="" target="_blank"><span class="icon-google-plus2"></span></a>
				</article>
			</article>
		</article>
		<article class="footfin">
			CONAXPORT © 2015 &nbsp;&nbsp;todos los derechos reservados &nbsp;- &nbsp;PBX (5) 841 733 &nbsp;&nbsp;Cúcuta - Colombia &nbsp;&nbsp;
			<a href="http://conaxport.com/" target="_blank">www.conaxport.com</a>
		</article>
	</footer>
</body>
</html>
<?php
	}
	else{
		echo "<script type='text/javascript'>";
			echo "var pagina='../erroradm.html';";
			echo "document.location.href=pagina;";
		echo "</script>";
	}
?>