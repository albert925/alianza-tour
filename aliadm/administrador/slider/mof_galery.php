<?php
	include '../../../config.php';
	session_start();
	if (isset($_SESSION['adm'])) {
		$seR=$_SESSION['adm'];
		$datad="SELECT * from administrador where id_adm=$seR";
		$sqlad=mysql_query($datad,$conexion) or die (mysql_error());
		while ($ad=mysql_fetch_array($sqlad)) {
			$idad=$ad['id_adm'];
			$usad=$ad['user_adm'];
			$tpad=$ad['tp_adm'];
		}
		$idR=$_GET['gl'];
		if ($idR=="") {
			echo "<script type='text/javascript'>";
				echo "alert('id imagen no disponible');";
				echo "var pagina='../slider';";
				echo "document.location.href=pagina;";
			echo "</script>";
		}
		else{
			$datos="SELECT * from slider_tx where id_slix=$idR";
			$sqldat=mysql_query($datos,$conexion) or die (mysql_error());
			$numdat=mysql_num_rows($sqldat);
			if ($numdat>0) {
				while ($dt=mysql_fetch_array($sqldat)) {
					$ttsl=$dt['tt_sli'];
					$sbsl=$dt['sb_sli'];
					$resl=$dt['re_sli'];
					$lksl=$dt['lk_sli'];
					$rtsl=$dt['rut_sli'];
				}
				$nomimg=split("/", $rtsl);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, maximun-scale=1" />
	<meta name="description" content="Datos <?php echo $usad ?>" />
	<meta name="keywords" content="admin" />
	<title>Slider Inicio | Alianza Tour</title>
	<link rel="icon" href="../../../imagenes/icono.png" />
	<link rel="image_src" href="../../../imagenes/logo.png" />
	<link rel="stylesheet" href="../../../css/normalize.css" />
	<link rel="stylesheet" href="../../../css/iconos/style.css" />
	<link rel="stylesheet" href="../../../css/style.css" />
	<link rel="stylesheet" href="../../../css/sty_adm.css" />
	<script src="../../../js/jquery_2_1_1.js"></script>
	<script src="../../../js/scripag.js"></script>
	<script src="../../../js/scadm.js"></script>
	<script src="../../../js/slider.js"></script>
</head>
<body>
	<header>
		<figure id="logo">
			<a href="../">
				<img src="../../../imagenes/logob.png" alt="logo" />
			</a>
		</figure>
		<div id="mn_mv"><span class="icon-menu"></span></div>
		<nav id="mnP">
			<ul>
				<li class="submen" data-num="1">
					<a class="sull" href="../slider">Slider I.</a>
					<ul class="children1">
						<li><a href="../proveedor">Proveedores</a></li>
						<li><a href="../cruceros">Cruceros</a></li>
					</ul>
				</li>
				<li><a href="../planes">Planes</a></li>
				<li><a href="../promocion">Promociones</a></li>
				<li class="submen" data-num="2"><a href="../documento">Documentos</a>
					<ul class="children2">
						<li><a href="../archivos">Archivos</a></li>
					</ul>
				</li>
				<li><a href="../"><?php echo "$usad"; ?></a></li>
				<li><a href="../../../cerrar">Salir</a></li>
			</ul>
		</nav>
	</header>
	<nav id="mnS">
		<a href="../slider">Ver Slider</a>
	</nav>
	<section class="padsec">
		<h1><?php echo "$ttsl"; ?></h1>
		<article id="automargen">
			<h2 class="hda">Cambiar Imagen</h2>
			<article>
				<form action="#" method="post" enctype="multipart/form-data" class="columninput" id="nslFf">
					<a href="../../../<?php echo $rtsl ?>" target="_blank"><?php echo $nomimg[2]; ?></a>
					<input type="text" id="idS" name="idS" value="<?php echo $idR ?>" required  style="display:none;" />
					<label>*<b>Imagen (resolución 1800px x 650px)</b></label>
					<input type="file" id="imusl" name="imusl" required />
					<div id="txA"></div>
					<input type="submit" value="Cambiar" id="fmimsli" />
				</form>
			</article>
			<h2 class="hda">Cambiar Datos</h2>
			<article>
				<form action="#" method="post" class="columninput">
					<label>*<b>Titulo</b></label>
					<input type="text" id="ttsl" name="ttsl" value="<?php echo $ttsl ?>" required />
					<label><b>Subtitulo</b></label>
					<input type="text" id="sbsl" name="sbsl" value="<?php echo $sbsl ?>" />
					<label><b>Link</b></label>
					<input type="url" id="llsl" name="llsl" value="<?php echo $lksl ?>" />
					<label><b>Texto</b> <span id="numcar">140</span> caracteres restantes</label>
					<textarea id="rssl" class="cantcar" name="rssl" rows="3" maxlength="140"><?php echo "$resl"; ?></textarea>
					<div id="txB"></div>
					<input type="submit" value="Modificar" id="mfdatsli" data-id="<?php echo $idR ?>" />
				</form>
			</article>
		</article>
	</section>
	<footer>
		<article class="foorflx">
			<article class="ar arr2">
				<h2>Alianza Tour </h2>
				<p>
					Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.
				</p>
				<p>
					Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de "Lorem Ipsum" va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo.
				</p>
			</article>
			<article class="ar">
				<h2>Contáctanos</h2>
				<div>Av 0 N 11-89 Local 105 CAOBOS</div>
				<div>Cúcuta Colombia</div>
				<div>Horario: Lunes-Viernes 9am-6:00pm</div>
				<div>Tel: 5 72 3840 - 324 567 2981</div>
				<div>Email: contacto@alianzatour.com</div>
			</article>
			<article class="ar">
				<article id="redes">
					<a href="" target="_blank"><span class="icon-facebook4"></span></a>
					<a href="" target="_blank"><span class="icon-instagram2"></span></a>
					<a href="" target="_blank"><span class="icon-youtube5"></span></a>
					<a href="" target="_blank"><span class="icon-google-plus2"></span></a>
				</article>
			</article>
		</article>
		<article class="footfin">
			CONAXPORT © 2015 &nbsp;&nbsp;todos los derechos reservados &nbsp;- &nbsp;PBX (5) 841 733 &nbsp;&nbsp;Cúcuta - Colombia &nbsp;&nbsp;
			<a href="http://conaxport.com/" target="_blank">www.conaxport.com</a>
		</article>
	</footer>
</body>
</html>
<?php
			}
			else{
				echo "<script type='text/javascript'>";
					echo "alert('slider no existe o ha sido eliminado');";
					echo "var pagina='../slider';";
					echo "document.location.href=pagina;";
				echo "</script>";
			}
		}
	}
	else{
		echo "<script type='text/javascript'>";
			echo "var pagina='../../erroradm.html';";
			echo "document.location.href=pagina;";
		echo "</script>";
	}
?>