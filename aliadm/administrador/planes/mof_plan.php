<?php
	include '../../../config.php';
	session_start();
	if (isset($_SESSION['adm'])) {
		$seR=$_SESSION['adm'];
		$datad="SELECT * from administrador where id_adm=$seR";
		$sqlad=mysql_query($datad,$conexion) or die (mysql_error());
		while ($ad=mysql_fetch_array($sqlad)) {
			$idad=$ad['id_adm'];
			$usad=$ad['user_adm'];
			$tpad=$ad['tp_adm'];
		}
		$idR=$_GET['pl'];
		if ($idR=="") {
			echo "<script type='text/javascript'>";
				echo "alert('id de plan no disponible');";
				echo "var pagina='../planes';";
				echo "document.location.href=pagina;";
			echo "</script>";
		}
		else{
			$datos="SELECT * from planes where id_pl=$idR";
			$sql_datos=mysql_query($datos,$conexion) or die (mysql_error());
			$numdatos=mysql_num_rows($sql_datos);
			if ($numdatos>0) {
				while ($dt=mysql_fetch_array($sql_datos)) {
					$tppl=$dt['tp_pl'];
					$nmpl=$dt['tit_pl'];
					$sbpl=$dt['sub_pl'];
					$xxpl=$dt['txt_pl'];
					$espl=$dt['es_pl'];
					$fepl=$dt['fe_pl'];
					$arpl=$dt['rut_pdf'];
				}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, maximun-scale=1" />
	<meta name="description" content="Datos <?php echo $usad ?>" />
	<meta name="keywords" content="admin" />
	<title>Plan <?php echo "$nmpl"; ?> | Alianza Tour</title>
	<link rel="icon" href="../../../imagenes/icono.png" />
	<link rel="image_src" href="../../../imagenes/logo.png" />
	<link rel="stylesheet" href="../../../css/normalize.css" />
	<link rel="stylesheet" href="../../../css/iconos/style.css" />
	<link rel="stylesheet" href="../../../css/style.css" />
	<link rel="stylesheet" href="../../../css/sty_adm.css" />
	<script src="../../../js/jquery_2_1_1.js"></script>
	<script src="../../../js/scripag.js"></script>
	<script src="../../../js/scadm.js"></script>
	<script src="../../../js/adplanes.js"></script>
	<script src="../../../ckeditor/ckeditor.js"></script>
</head>
<body>
	<header>
		<figure id="logo">
			<a href="../">
				<img src="../../../imagenes/logob.png" alt="logo" />
			</a>
		</figure>
		<div id="mn_mv"><span class="icon-menu"></span></div>
		<nav id="mnP">
			<ul>
				<li class="submen" data-num="1">
					<a href="../slider">Slider I.</a>
					<ul class="children1">
						<li><a href="../proveedor">Proveedores</a></li>
						<li><a href="../cruceros">Cruceros</a></li>
					</ul>
				</li>
				<li><a class="sull" href="../planes">Planes</a></li>
				<li><a href="../promocion">Promociones</a></li>
				<li class="submen" data-num="2"><a href="../documento">Documentos</a>
					<ul class="children2">
						<li><a href="../archivos">Archivos</a></li>
					</ul>
				</li>
				<li><a href="../"><?php echo "$usad"; ?></a></li>
				<li><a href="../../../cerrar">Salir</a></li>
			</ul>
		</nav>
	</header>
	<nav id="mnS">
		<a href="../planes">Ver Planes</a>
		<a href="tipos.php">Tipo planes</a>
		<a href="images_planes.php">Imagenes planes</a>
	</nav>
	<section>
		<h1><?php echo "$nmpl"; ?></h1>
		<article id="automargen" class="btpG">
			<form action="#" enctype="multipart/form-data" 
				method="post" class="columninput" id="arch">
				<h2>Cambiar Archivo</h2>
				<a href="../../../<?php echo $arpl ?>"><?php echo $arpl ?></a>
				<input type="text" id="idp" name="idp" value="<?php echo $idR ?>" required style="display:none;" />
				<input type="file" id="pfp" name="pfp" required />
				<div id="txac"></div>
				<input type="submit" value="modificar" id="camarc" />
			</form>
		</article>
		<article id="automargen" class="btpG">
			<form action="modiff_plan.php" method="post" class="columninput">
				<h2>Datos</h2>
				<input type="text" id="id" name="id" value="<?php echo $idR ?>" required style="display:none;" />
				<label>*<b>Del tipo</b></label>
				<select id="tpsl" name="tpsl">
					<option value="0">Selecione</option>
					<?php
						$tdTP="SELECT * from tip_planes order by id_tppl desc";
						$sql_tp=mysql_query($tdTP,$conexion) or die (mysql_error());
						while ($Otp=mysql_fetch_array($sql_tp)) {
							$idtp=$Otp['id_tppl'];
							$nmtp=$Otp['nm_tppl'];
							if ($idtp==$tppl) {
								$seltipo="selected";
							}
							else{
								$seltipo="";
							}
					?>
					<option value="<?php echo $idtp ?>" <?php echo $seltipo ?>><?php echo "$nmtp"; ?></option>
					<?php
						}
					?>
				</select>
				<label>*<b>Título</b></label>
				<input type="text" id="ntpl" name="ntpl" value="<?php echo $nmpl ?>" required />
				<label><b>Subtitulo</b></label>
				<input type="text" id="nspl" name="nspl" value="<?php echo $sbpl ?>" />
				<label><b>Texto</b></label>
				<textarea id="editor1" name="nxpl"><?php echo "$xxpl"; ?></textarea>
				<script>
					CKEDITOR.replace('nxpl');
				</script>
				<div id="txA"></div>
				<input type="submit" value="Modificar" id="nvcrual" />
			</form>
		</article>
	</section>
	<footer>
		<article class="foorflx">
			<article class="ar arr2">
				<h2>Alianza Tour </h2>
				<p>
					Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.
				</p>
				<p>
					Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de "Lorem Ipsum" va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo.
				</p>
			</article>
			<article class="ar">
				<h2>Contáctanos</h2>
				<div>Av 0 N 11-89 Local 105 CAOBOS</div>
				<div>Cúcuta Colombia</div>
				<div>Horario: Lunes-Viernes 9am-6:00pm</div>
				<div>Tel: 5 72 3840 - 324 567 2981</div>
				<div>Email: contacto@alianzatour.com</div>
			</article>
			<article class="ar">
				<article id="redes">
					<a href="" target="_blank"><span class="icon-facebook4"></span></a>
					<a href="" target="_blank"><span class="icon-instagram2"></span></a>
					<a href="" target="_blank"><span class="icon-youtube5"></span></a>
					<a href="" target="_blank"><span class="icon-google-plus2"></span></a>
				</article>
			</article>
		</article>
		<article class="footfin">
			CONAXPORT © 2015 &nbsp;&nbsp;todos los derechos reservados &nbsp;- &nbsp;PBX (5) 841 733 &nbsp;&nbsp;Cúcuta - Colombia &nbsp;&nbsp;
			<a href="http://conaxport.com/" target="_blank">www.conaxport.com</a>
		</article>
	</footer>
</body>
</html>
<?php
			}
			else{
				echo "<script type='text/javascript'>";
					echo "alert('plan no existe o ha sido eliminado');";
					echo "var pagina='../planes';";
					echo "document.location.href=pagina;";
				echo "</script>";
			}
		}
	}
	else{
		echo "<script type='text/javascript'>";
			echo "var pagina='../../erroradm.html';";
			echo "document.location.href=pagina;";
		echo "</script>";
	}
?>