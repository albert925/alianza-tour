<?php
	include '../../../config.php';
	session_start();
	if (isset($_SESSION['adm'])) {
		$seR=$_SESSION['adm'];
		$datad="SELECT * from administrador where id_adm=$seR";
		$sqlad=mysql_query($datad,$conexion) or die (mysql_error());
		while ($ad=mysql_fetch_array($sqlad)) {
			$idad=$ad['id_adm'];
			$usad=$ad['user_adm'];
			$tpad=$ad['tp_adm'];
		}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, maximun-scale=1" />
	<meta name="description" content="Datos <?php echo $usad ?>" />
	<meta name="keywords" content="admin" />
	<title>Planes | Alianza Tour</title>
	<link rel="icon" href="../../../imagenes/icono.png" />
	<link rel="image_src" href="../../../imagenes/logo.png" />
	<link rel="stylesheet" href="../../../css/normalize.css" />
	<link rel="stylesheet" href="../../../css/iconos/style.css" />
	<link rel="stylesheet" href="../../../css/style.css" />
	<link rel="stylesheet" href="../../../css/sty_adm.css" />
	<script src="../../../js/jquery_2_1_1.js"></script>
	<script src="../../../js/scripag.js"></script>
	<script src="../../../js/scadm.js"></script>
	<script src="../../../js/adplanes.js"></script>
	<script src="../../../ckeditor/ckeditor.js"></script>
</head>
<body>
	<header>
		<figure id="logo">
			<a href="../">
				<img src="../../../imagenes/logob.png" alt="logo" />
			</a>
		</figure>
		<div id="mn_mv"><span class="icon-menu"></span></div>
		<nav id="mnP">
			<ul>
				<li class="submen" data-num="1">
					<a href="../slider">Slider I.</a>
					<ul class="children1">
						<li><a href="../proveedor">Proveedores</a></li>
						<li><a href="../cruceros">Cruceros</a></li>
					</ul>
				</li>
				<li><a class="sull" href="../planes">Planes</a></li>
				<li><a href="../promocion">Promociones</a></li>
				<li class="submen" data-num="2"><a href="../documento">Documentos</a>
					<ul class="children2">
						<li><a href="../archivos">Archivos</a></li>
					</ul>
				</li>
				<li><a href="../"><?php echo "$usad"; ?></a></li>
				<li><a href="../../../cerrar">Salir</a></li>
			</ul>
		</nav>
	</header>
	<nav id="mnS">
		<a id="abl" href="#">Nuevo Plan</a>
		<a href="tipos.php">Tipo planes</a>
		<a href="images_planes.php">Imagenes planes</a>
	</nav>
	<section>
		<h1>Planes</h1>
		<article id="mosG" class="coult">
			<article id="automargen">
				<form action="../../../new_plan.php" enctype="multipart/form-data" method="post" class="columninput">
					<h2>Nuevo plan</h2>
					<label>*<b>Del tipo</b></label>
					<select id="tpsl" name="tpsl">
						<option value="0">Selecione</option>
						<?php
							$tdTP="SELECT * from tip_planes order by id_tppl desc";
							$sql_tp=mysql_query($tdTP,$conexion) or die (mysql_error());
							while ($Otp=mysql_fetch_array($sql_tp)) {
								$idtp=$Otp['id_tppl'];
								$nmtp=$Otp['nm_tppl'];
						?>
						<option value="<?php echo $idtp ?>"><?php echo "$nmtp"; ?></option>
						<?php
							}
						?>
					</select>
					<label>*<b>Título</b></label>
					<input type="text" id="ntpl" name="ntpl" required />
					<label><b>Subtitulo</b></label>
					<input type="text" id="nspl" name="nspl" />
					<label>*<b>Archivo pdf</b></label>
					<input type="file" id="pfpl" name="pfpl" required />
					<label><b>Texto</b></label>
					<textarea id="editor1" name="nxpl"></textarea>
					<script>
						CKEDITOR.replace('nxpl');
					</script>
					<div id="txA"></div>
					<input type="submit" value="Ingresar" id="nvcrual" />
				</form>
			</article>
		</article>
		<article id="automargen" class="flB">
			<?php
				error_reporting(E_ALL ^ E_NOTICE);
				$tamno_pagina=15;
				$pagina= $_GET['pagina'];
				if (!$pagina) {
					$inicio=0;
					$pagina=1;
				}
				else{
					$inicio= ($pagina - 1)*$tamno_pagina;
				}
				$ssql="SELECT * from planes order by id_pl desc";
				$rs=mysql_query($ssql,$conexion) or die (mysql_error());
				$num_total_registros= mysql_num_rows($rs);
				$total_paginas= ceil($num_total_registros / $tamno_pagina);
				$gsql="SELECT * from planes order by id_pl desc limit $inicio, $tamno_pagina";
				$impsql=mysql_query($gsql,$conexion) or die (mysql_error());
				while ($gh=mysql_fetch_array($impsql)) {
					$idpl=$gh['id_pl'];
					$tppl=$gh['tp_pl'];
					$nmpl=$gh['tit_pl'];
					$sbpl=$gh['sub_pl'];
					$xxpl=$gh['txt_pl'];
					$espl=$gh['es_pl'];
					$fepl=$gh['fe_pl'];
					$primerimg="SELECT * from images_planes where pl_id=$idpl order by id_img_pl asc limit 1";
					$sql_img=mysql_query($primerimg,$conexion) or die (mysql_error());
					$numimg=mysql_num_rows($sql_img);
					if ($numimg>0) {
						while ($tr=mysql_fetch_array($sql_img)) {
							$idmg=$tr['id_img_pl'];
							$rtmg=$tr['rut_pl'];
						}
					}
					else{
						$idmg=0;
						$rtmg="imagenes/predeterminado.png";
					}
			?>
			<figure id="ar<?php echo $idpl ?>">
				<h2><?php echo "$nmpl"; ?></h2>
				<img src="../../../<?php echo $rtmg ?>" alt="<?php echo $nmpl ?>" />
				<figcaption class="columninput">
					<a id="dslmf" href="mof_plan.php?pl=<?php echo $idpl ?>">Modificar</a>
					<a id="dslmf" href="planes_images.php?pl=<?php echo $idpl ?>">Imágenes</a>
					<a class="dull" href="borr_plan.php" data-id="<?php echo $idpl ?>">Borrar</a>
				</figcaption>
			</figure>
			<?php
				}
			?>
		</article>
		<article id="automargen">
			<br />
			<b>Páginas: </b>
			<?php
				//muestro los distintos indices de las paginas
				if ($total_paginas>1) {
					for ($i=1; $i <=$total_paginas ; $i++) { 
						if ($pagina==$i) {
							//si muestro el indice del la pagina actual, no coloco enlace
				?>
					<b><?php echo $pagina." "; ?></b>
				<?php
						}
						else{
							//si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página 
				?>
							<a href="index.php?pagina=<?php echo $i ?>"><?php echo "$i"; ?></a>

				<?php
						}
					}
				}
			?>
		</article>
	</section>
	<footer>
		<article class="foorflx">
			<article class="ar arr2">
				<h2>Alianza Tour </h2>
				<p>
					Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.
				</p>
				<p>
					Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de "Lorem Ipsum" va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo.
				</p>
			</article>
			<article class="ar">
				<h2>Contáctanos</h2>
				<div>Av 0 N 11-89 Local 105 CAOBOS</div>
				<div>Cúcuta Colombia</div>
				<div>Horario: Lunes-Viernes 9am-6:00pm</div>
				<div>Tel: 5 72 3840 - 324 567 2981</div>
				<div>Email: contacto@alianzatour.com</div>
			</article>
			<article class="ar">
				<article id="redes">
					<a href="" target="_blank"><span class="icon-facebook4"></span></a>
					<a href="" target="_blank"><span class="icon-instagram2"></span></a>
					<a href="" target="_blank"><span class="icon-youtube5"></span></a>
					<a href="" target="_blank"><span class="icon-google-plus2"></span></a>
				</article>
			</article>
		</article>
		<article class="footfin">
			CONAXPORT © 2015 &nbsp;&nbsp;todos los derechos reservados &nbsp;- &nbsp;PBX (5) 841 733 &nbsp;&nbsp;Cúcuta - Colombia &nbsp;&nbsp;
			<a href="http://conaxport.com/" target="_blank">www.conaxport.com</a>
		</article>
	</footer>
</body>
</html>
<?php
	}
	else{
		echo "<script type='text/javascript'>";
			echo "var pagina='../../erroradm.html';";
			echo "document.location.href=pagina;";
		echo "</script>";
	}
?>