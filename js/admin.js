$(document).on("ready",inicio_admin);
function inicio_admin () {
	$("#ingi").on("click",ingad);
	$("#camA").on("click",camA);
	$("#camB").on("click",camB);
}
var bien={color:"#007232"}
var normal={color:"#000"}
var mal={color:"#CD0000"}
function ingad () {
	var la=$("#usa").val();
	var lb=$("#psa").val();
	if (la=="") {
		$("#txA").css(mal).text("Ingrese el nombre de usuario");
		return false;
	}
	else{
		if (lb=="") {
			$("#txA").css(mal).text("Ingrese la contraseña");
			return false;
		}
		else{
			$("#txA").css(normal).text("");
			$("#txA").prepend("<center><img src='../imagenes/loadingb.gif' alt='loading' /></center>");
			$.post("ing_ad.php",{a:la,b:lb},resad);
			return false;
		}
	}
}
function resad (res) {
	if (res=="2") {
		$("#txA").css(mal).text("Usuario o contraseña incorrectos");
			return false;
	}
	else{
		if (res=="3") {
			$("#txA").css(bien).text("Ingresando..");
			window.location.href="administrador";
		}
		else{
			$("#txA").css(mal).html(res);
			return false;
		}
	}
}
function camA () {
	var ida=$(this).attr("data-id");
	var us=$("#fus").val();
	if (us=="") {
		$("#txA").css(mal).text("Ingrese el nombre de usuario");
	}
	else{
		$("#txA").css(normal).text("");
		$("#txA").prepend("<center><img src='../../imagenes/loadingb.gif' alt='loading' /></center>");
		$.post("mof_usad.php",{fa:ida,a:us},function (ras) {
			if (ras=="2") {
				$("#txA").css(mal).text("Nombre de usuario ya existe");
			}
			else{
				if (ras=="3") {
					$("#txA").css(bien).text("Usuario cambiado");
					location.reload(20);
				}
				else{
					$("#txA").css(mal).html(ras);
				}
			}
		});
	}
}
function camB () {
	var idb=$(this).attr("data-id");
	var uac=$("#cac").val();
	var na=$("#cna").val();
	var nb=$("#cnb").val();
	if (uac=="") {
		$("#txB").css(mal).text("Ingrese la contraseña actual");
	}
	else{
		if (na=="" || na.length<6) {
			$("#txB").css(mal).text("Contraseña mínimo 6 dígitos");
		}
		else{
			if (nb!=na) {
				$("#txB").css(mal).text("Contraseñas no coinciden");
			}
			else{
				$("#txB").css(normal).text("");
				$("#txB").css(mal).prepend("<center><img src='../../imagenes/loadingb.gif' alt='loading' /></center>");
				$.post("mof_pass.php",{fa:idb,a:uac,b:na},function (usr) {
					if (usr=="2") {
						$("#txB").css(mal).text("Contraseña actual incorrecta");
					}
					else{
						if (usr=="3") {
							$("#txB").css(bien).text("Contraseña cambiada");
							setTimeout(direccionar,2500);
						}
						else{
							$("#txB").css(mal).html(usr);
						}
					}
				});
			}
		}
	}
}
function direccionar () {
	window.location.href="../../cerrar";
}