$(inicioarchivo);
function inicioarchivo () {
	$("#nvtp").on("click",newtipo);
	$("#nvarch").on("click",nuewarchivo);
	$(".camtp").on("click",camtipo);
}
var bien={color:"#007232"}
var normal={color:"#000"}
var mal={color:"#CD0000"}
function es_imagen (tipederf) {
	switch(tipederf.toLowerCase()){
		case 'jpg':
			return true;
			break;
		case 'gif':
			return true;
			break;
		case 'png':
			return true;
			break;
		case 'jpeg':
			return true;
			break;
		default:
			return false;
			break;
	}
}
function newtipo () {
	var na=$("#ntpl").val();
	var nb=$("#dctp").val();
	if (nb=="0" || nb=="") {
		$("#txA").css(mal).text("Selecione del documento");
		return false;
	}
	else{
		if (na=="") {
			$("#txA").css(mal).text("ingrese el nombre");
			return false;
		}
		else{
			$("#txA").css(normal).text("");
			$("#txA").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' /></center>");
			$.post("new_tipo.php",{a:na,b:nb},rstp);
			return false;
	}
	}
}
function rstp (rs) {
	if (rs=="2") {
		$("#txA").css(mal).text("Nombre ya existe");
		return false;
	}
	else{
		if (rs=="3") {
			$("#txA").css(bien).text("ingresado");
			location.reload(20);
		}
		else{
			$("#txA").css(mal).html(rs);
			return false;
		}
	}
}
function camtipo () {
	var ida=$(this).attr("data-id");
	var nf=$("#camtt_"+ida).val();
	var dc=$("#fd_"+ida).val();
	$("#txb_"+ida).fadeIn();
	if (dc=="0" || dc=="") {
		("#txb_"+ida).css(mal).text("Selecione del documento");
	}
	else{
		if (nf=="") {
			$("#txb_"+ida).css(mal).text("ingrese el nombre");
		}
		else{
			$("#txb_"+ida).css(normal).text("");
			$("#txb_"+ida).prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' /></center>");
			$.post("mof_tipo.php",{fa:ida,a:nf,b:dc},function (ts) {
				if (ts=="2") {
					$("#txb_"+ida).css(bien).text("Modificado");
					$("#txb_"+ida).fadeOut(1500);
				}
				else{
					$("#txb_"+ida).css(mal).html(ts);
				}
			});
		}
	}
}
function nuewarchivo () {
	var ara=$("#tpar").val();
	var arb=$("#nar").val();
	var arc=$("#flar")[0].files[0];
	var namearc=arc.name;
	var extearc=namearc.substring(namearc.lastIndexOf('.')+1);
	var tamarc=arc.size;
	var tipoarc=arc.type;
	if (ara=="0" || ara=="") {
		$("#txA").css(mal).text("Selecione el tipo");
		return false;
	}
	else{
		if (arb=="") {
			$("#txA").css(mal).text("Selecione el nombre del archivo");
			return false
		}
		else{
			$("#txA").css(normal).text("");
			var formu=new FormData($("#far")[0]);
			$("#txA").css(normal).text("");
			$.ajax({
				url: '../../../nuevoarchivo.php',
				type: 'POST',
				data: formu,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#txA").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
				},
				success:reulimg,
				error:function () {
					$("#txA").css(mal).text("Ocurrió un error");
					$("#txA").fadeIn();$("#txA").fadeOut(3000);
				}
			});	
			return false
		}
	}
}
function reulimg (resig) {
	if (resig=="2") {
		$("#txA").css(mal).text("Carpeta sin permisos");
		$("#txA").fadeIn();$("#txA").fadeOut(3000);
		return false;
	}
	else{
		if (resig=="3") {
			$("#txA").css(mal).text("Tamaño no permitido");
			$("#txA").fadeIn();$("#txA").fadeOut(3000);
			return false;
		}
		else{
			if (resig=="4") {
				$("#txA").css(mal).text("Carpeta sin permisos");
				$("#txA").fadeIn();$("#txA").fadeOut(3000);
				return false;
			}
			else{
				if (resig=="5") {
					$("#txA").css(bien).text("Archivo subido");
					$("#txA").fadeIn();$("#txA").fadeOut(3000);
					location.reload(20);
				}
				else{
					$("#txA").css(mal).html(resig);
					$("#txA").fadeIn();
					return false;
				}
			}
		}
	}
}