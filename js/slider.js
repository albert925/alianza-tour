$(document).on("ready",inicio_slider);
function inicio_slider () {
	$("#nvslid").on("click",subirslide);
	$("#fmimsli").on("click",camimgsl);
	$("#mfdatsli").on("click",datslid);
	$("#nvpval").on("click",nuv_prov);
	$("#nvcrual").on("click",nuevo_crucero);
	$(".camU").on("click",cambiadat);
}
var bien={color:"#007232"}
var normal={color:"#000"}
var mal={color:"#CD0000"}
function es_imagen (tipederf) {
	switch(tipederf.toLowerCase()){
		case 'jpg':
			return true;
			break;
		case 'gif':
			return true;
			break;
		case 'png':
			return true;
			break;
		case 'jpeg':
			return true;
			break;
		default:
			return false;
			break;
	}
}
function subirslide () {
	var sla=$("#ttsl").val();
	var slb=$("#sbsl").val();
	var slc=$("#llsl").val();
	var sle=$("#rssl").val();
	var sld=$("#imsl")[0].files[0];
	var namesld=sld.name;
	var extesld=namesld.substring(namesld.lastIndexOf('.')+1);
	var tamsld=sld.size;
	var tiposld=sld.type;
	if (sla=="") {
		$("#txA").css(mal).text("Ingrese el título");
		$("#ttsl").focus();
	}
	else{
		if (!es_imagen(extesld)) {
			$("#txA").css(mal).text("Tipo de imagen no permitido");
			return false;
		}
		else{
			var formu=new FormData($("#nsl")[0]);
			$("#txA").css(normal).text("");
			$.ajax({
				url: '../../../nuevoimggalery.php',
				type: 'POST',
				data: formu,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#txA").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
				},
				success:reulimg,
				error:function () {
					$("#txA").css(mal).text("Ocurrió un error");
					$("#txA").fadeIn();$("#txA").fadeOut(3000);
				}
			});	
			return false;
		}
	}
}
function reulimg (resig) {
	if (resig=="2") {
		$("#txA").css(mal).text("Carpeta sin permisos o resolución de imagen no permitido");
		$("#txA").fadeIn();$("#txA").fadeOut(3000);
		return false;
	}
	else{
		if (resig=="3") {
			$("#txA").css(mal).text("Tamaño no permitido");
			$("#txA").fadeIn();$("#txA").fadeOut(3000);
			return false;
		}
		else{
			if (resig=="4") {
				$("#txA").css(mal).text("Carpeta sin permisos o resolución de imagen no permitido");
				$("#txA").fadeIn();$("#txA").fadeOut(3000);
				return false;
			}
			else{
				if (resig=="5") {
					$("#txA").css(bien).text("Imagen subida");
					$("#txA").fadeIn();$("#txA").fadeOut(3000);
					location.reload(20);
				}
				else{
					$("#txA").css(mal).html(resig);
					$("#txA").fadeIn();
					return false;
				}
			}
		}
	}
}
function camimgsl () {
	var ida=$("#idS").val();
	var sb=$("#imusl")[0].files[0];
	var namesb=sb.name;
	var extesb=namesb.substring(namesb.lastIndexOf('.')+1);
	var tamsb=sb.size;
	var tiposb=sb.type;
	if (ida=="" || ida=="0") {
		$("#txA").css(mal).text("Id de slide no disponible");
		return false;
	}
	else{
		if (!es_imagen(extesb)) {
			$("#txA").css(mal).text("Tipo de imagen no permitido");
			return false;
		}
		else{
			$("#txA").css(normal).text("");
			var formb=new FormData($("#nslFf")[0]);
			$.ajax({
				url: '../../../moffimggalery.php',
				type: 'POST',
				data: formb,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#txA").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
				},
				success:reulimg,
				error:function () {
					$("#txA").css(mal).text("Ocurrió un error");
					$("#txA").fadeIn();$("#txA").fadeOut(3000);
				}
			});
			return false;
		}
	}
}
function datslid () {
	var idb=$(this).attr("data-id");
	var sfa=$("#ttsl").val();
	var sfb=$("#sbsl").val();
	var sfc=$("#llsl").val();
	var sfd=$("#rssl").val();
	if (sfa=="") {
		$("#txB").css(mal).text("ingrese el titulo");
		$("#ttsl").focus();
		return false;
	}
	else{
		$("#txB").css(normal).text("");
		$("#txB").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
		$.post("mof_datsli.php",{fa:idb,a:sfa,b:sfb,c:sfc,d:sfd},reldat);
		return false;
	}
}
function reldat (rsy) {
	if (rsy=="2") {
		$("#txB").css(bien).text("Modificado");
		location.reload(20);
	}
	else{
		$("#txB").css(mal).html(rsy);
		return false;
	}
}
function nuv_prov () {
	var paa=$("#ttal").val();
	var pab=$("#llal").val();
	var pac=$("#imal")[0].files[0];
	var namepac=pac.name;
	var extepac=namepac.substring(namepac.lastIndexOf('.')+1);
	var tampac=pac.size;
	var tipopac=pac.type;
	if (paa=="") {
		$("#txA").css(mal).text("Ingrese el título");
		$("#ttal").focus();
		return false;
	}
	else{
		if (!es_imagen(extepac)) {
			$("#txA").css(mal).text("Tipod e imagen no permitido");
			return false;
		}
		else{
			$("#txA").css(normal).text("");
			var formt=new FormData($("#npv")[0]);
			$.ajax({
				url: '../../../newproveedor.php',
				type: 'POST',
				data: formt,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#txA").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
				},
				success:reulimg,
				error:function () {
					$("#txA").css(mal).text("Ocurrió un error");
					$("#txA").fadeIn();$("#txA").fadeOut(3000);
				}
			});
			return false;
		}
	}
}
function nuevo_crucero () {
	var icu=$("#imcru")[0].files[0];
	var nameicu=icu.name;
	var exteicu=nameicu.substring(nameicu.lastIndexOf('.')+1);
	var tamicu=icu.size;
	var tipoicu=icu.type;
	if (!es_imagen(exteicu)) {
		$("#txA").css(mal).text("tipo de imagen no permitido");
		return false;
	}
	else{
		$("#txA").css(normal).text("");
		var formt=new FormData($("#ncru")[0]);
		$.ajax({
			url: '../../../newcrucero.php',
			type: 'POST',
			data: formt,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend:function () {
				$("#txA").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
			},
			success:reulimg,
			error:function () {
				$("#txA").css(mal).text("Ocurrió un error");
				$("#txA").fadeIn();$("#txA").fadeOut(3000);
			}
		});
		return false;
	}
}
function cambiadat () {
	var fid=$(this).attr("data-id");
	var lkf=$("#furl_"+fid).val();
	if (lkf=="") {
		$("#fa_"+fid).css(mal).text("ingrese la url")
	}
	else{
		$("#fa_"+fid).css(normal).text("")
			.prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
		$.post("cambdat_prove.php",{fa:fid,a:lkf},function (res) {
			if (res == "2") {
				$("#fa_"+fid).css(bien).text("modificado")
				location.reload(20);
			}
			else{
				$("#fa_"+fid).css(mal).html(res)
			}
		})
	}
}