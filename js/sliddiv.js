$(document).on("ready",icionowl);
function icionowl () {
	 $('.owl-carousel').owlCarousel({
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:false,
		dots:false,
		loop:true,
		margin:0,
		nav:false,
		items:1,
		onInitialize:siesuno
	});
	$('.owl-carousel-b').owlCarousel({
		autoplay:true,
		autoplayTimeout:7000,
		autoplayHoverPause:true,
		loop:false,
		margin:10,
		responsiveClass:true,
		dots:false,
		nav:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			},
			1200:{
				items:4
			}
		}
	});
	//error o bug si es en item 1 con un solo elemento
	function siesuno (event) {
		var unr=$('.owl-carousel .item');
		if (unr.length<=1) {
			this.settings.loop=false;
		}
	}
	$(".owl-prev").text("<");
	$(".owl-next").text(">");
}