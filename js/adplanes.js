$(document).on("ready",inicio_planes);
function inicio_planes () {
	$("#nvtp").on("click",nuevo_tipo);
	$("#nvcrual").on("click",validaplanes);
	$("#nvimgpl").on("click",nuevoimagenplan);
	$("#camarc").on("click",cambiopdf)
	$(".camtp").on("click",camb_tipo);
	$(".yuf").on("click",mof_imgtptp);
}
var bien={color:"#007232"}
var normal={color:"#000"}
var mal={color:"#CD0000"}
function es_imagen (tipederf) {
	switch(tipederf.toLowerCase()){
		case 'jpg':
			return true;
			break;
		case 'gif':
			return true;
			break;
		case 'png':
			return true;
			break;
		case 'jpeg':
			return true;
			break;
		default:
			return false;
			break;
	}
}
function nuevo_tipo () {
	var nm=$("#ntpl").val();
	var ig=$("#igpl")[0].files[0];
	var nameig=ig.name;
	var exteig=nameig.substring(nameig.lastIndexOf('.')+1);
	var tamig=ig.size;
	var tipoig=ig.type;
	if (nm=="") {
		$("#txA").css(mal).text("Ingrese el nombre");
		return false;
	}
	else{
		if (!es_imagen(exteig)) {
			$("#txA").css(mal).text("Tipo de imagen no permitido");
			return false;
		}
		else{
			$("#txA").css(normal).text("");
			var forig=new FormData($("#ggpl")[0]);
			$.ajax({
				url: '../../../nuevotipoplan.php',
				type: 'POST',
				data: forig,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#txA").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
				},
				success:resulti,
				error:function () {
					$("#txA").css(mal).text("Ocurrió un error");
					$("#txA").fadeIn();$("#txA").fadeOut(3000);
				}
			});	
			//$.post("new_tipo.php",{a:nm},resulti);
			return false;
		}
	}
}
function resulti (resig) {
	if (resig=="2") {
		$("#txA").css(mal).text("Carpeta sin permisos o resolución de imagen no permitido");
		$("#txA").fadeIn();$("#txA").fadeOut(3000);
		return false;
	}
	else{
		if (resig=="3") {
			$("#txA").css(mal).text("Tamaño no permitido");
			$("#txA").fadeIn();$("#txA").fadeOut(3000);
			return false;
		}
		else{
			if (resig=="4") {
				$("#txA").css(mal).text("Carpeta sin permisos o resolución de imagen no permitido");
				$("#txA").fadeIn();$("#txA").fadeOut(3000);
				return false;
			}
			else{
				if (resig=="5") {
					$("#txA").css(bien).text("Ingresado");
					$("#txA").fadeIn();$("#txA").fadeOut(3000);
					location.reload(20);
				}
				else{
					$("#txA").css(mal).html(resig);
					$("#txA").fadeIn();
					return false;
				}
			}
		}
	}
}
function camb_tipo () {
	var ida=$(this).attr("data-id");
	var nmtp=$("#camtt_"+ida).val();
	$("#txb_"+ida).fadeIn();
	if (nmtp=="") {
		$("#txb_"+ida).css(mal).text("Ingrese el nombre");
	}
	else{
		$("#txb_"+ida).css(normal).text("");
		$.post("mof_tp.php",{fa:ida,a:nmtp},function (res) {
			if (res=="2") {
				$("#txb_"+ida).css(bien).text("modificado");
				$("#txb_"+ida).fadeOut(2000);
			}
			else{
				$("#txb_"+ida).css(mal).html(res);
			}
		});
	}
}
function validaplanes () {
	var sel1=$("#tpsl").val();
	if (sel1=="0" || sel1=="") {
		alert("Selecione tipo de planes");
		$("#txA").css(mal).text("Selecione tipo de planes");
		return false;
	}
	else{
		$("#txA").css(normal).text("");
		return true;
	}
}
function nuevoimagenplan () {
	var iga=$("#idpla").val();
	var igb=$("#igpl")[0].files[0];
	var nameigb=igb.name;
	var exteigb=nameigb.substring(nameigb.lastIndexOf('.')+1);
	var tamigb=igb.size;
	var tipoigb=igb.type;
	if (iga=="0" || iga=="") {
		$("#jil").css(mal).text("id de plan no disponible");
		return false;
	}
	else{
		if (!es_imagen(exteigb)) {
			$("#jil").css(mal).text("tipo de imagen no permitido");
			return false;
		}
		else{
			$("#jil").css(normal).text("");
			var formu=new FormData($("#pligig")[0]);
			$.ajax({
				url: '../../../nuevoimgplanes.php',
				type: 'POST',
				data: formu,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#jil").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
				},
				success:function (resig) {
					if (resig=="2") {
						$("#jil").css(mal).text("Carpeta sin permisos o resolución de imagen no permitido");
						$("#jil").fadeIn();$("#jil").fadeOut(3000);
						return false;
					}
					else{
						if (resig=="3") {
							$("#jil").css(mal).text("Tamaño no permitido");
							$("#jil").fadeIn();$("#jil").fadeOut(3000);
							return false;
						}
						else{
							if (resig=="4") {
								$("#jil").css(mal).text("Carpeta sin permisos o resolución de imagen no permitido");
								$("#jil").fadeIn();$("#jil").fadeOut(3000);
								return false;
							}
							else{
								if (resig=="5") {
									$("#jil").css(bien).text("Imagen subida");
									$("#jil").fadeIn();$("#jil").fadeOut(3000);
									window.location.href="planes_images.php?pl="+iga;
								}
								else{
									$("#jil").css(mal).html(resig);
									$("#jil").fadeIn();
									return false;
								}
							}
						}
					}
				},
				error:function () {
					$("#jil").css(mal).text("Ocurrió un error");
					$("#jil").fadeIn();$("#jil").fadeOut(3000);
				}
			});
			return false;
		}
	}
}
function mof_imgtptp () {
	var idc=$(this).attr("data-id");
	var inid=$("#igf_"+idc).val();
	var lit=$("#fif_"+idc)[0].files[0];
	var namelit=lit.name;
	var extelit=namelit.substring(namelit.lastIndexOf('.')+1);
	var tamlit=lit.size;
	var tipolit=lit.type;
	$("#si_"+idc).fadeIn();
	if (inid=="0" || inid=="") {
		$("#si_"+idc).css(mal).text("id de tipo no disponible");
		return false;
	}
	else{
		if (!es_imagen(extelit)) {
			$("#si_"+idc).css(mal).text("tipo de imagen no permitido");
			return false;
		}
		else{
			$("#si_"+idc).css(normal).text("");
			var kig=new FormData($("#fpl_"+idc)[0]);
			$.ajax({
				url: '../../../mofiimgtipoplan.php',
				type: 'POST',
				data:kig,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#si_"+idc).prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
				},
				success:function (sets) {
					if (sets=="2") {
						$("#si_"+idc).css(mal).text("Carpeta sin permisos o resolución de imagen no permitido");
						$("#si_"+idc).fadeIn();$("#si_"+idc).fadeOut(3000);
						return false;
					}
					else{
						if (sets=="3") {
							$("#si_"+idc).css(mal).text("Tamaño no permitido");
							$("#si_"+idc).fadeIn();$("#si_"+idc).fadeOut(3000);
							return false;
						}
						else{
							if (sets=="4") {
								$("#si_"+idc).css(mal).text("Carpeta sin permisos o resolución de imagen no permitido");
								$("#si_"+idc).fadeIn();$("#si_"+idc).fadeOut(3000);
								return false;
							}
							else{
								if (sets=="5") {
									$("#si_"+idc).css(bien).text("Imagen modificado");
									$("#si_"+idc).fadeIn();$("#si_"+idc).fadeOut(3000);
									$("#ar"+idc+" img").attr("src","../../../imagenes/tipos_planes/"+namelit);
									return false;
								}
								else{
									$("#si_"+idc).css(mal).html(sets);
									$("#si_"+idc).fadeIn();
									return false;
								}
							}
						}
					}
				},
				error:function () {
					$("#si_"+idc).css(mal).text("Ocurrió un error");
					$("#si_"+idc).fadeIn();$("#si_"+idc).fadeOut(3000);
				}
			});	
			return false;
		}
	}
}
function cambiopdf () {
	var idg=$("#idp").val()
	var pdf=$("#pfp")[0].files[0];
	var namepdf=pdf.name;
	var extepdf=namepdf.substring(namepdf.lastIndexOf('.')+1);
	var tampdf=pdf.size;
	var tipopdf=pdf.type;
	if (idg=="" || idg=="0") {
		$("#txac").css(mal).text("id de plan no disponible");
		return false;
	}
	else{
		$("#txac").css(normal).text("");
		var forig=new FormData($("#arch")[0]);
		$.ajax({
			url: '../../../mofarchiplan.php',
			type: 'POST',
			data: forig,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend:function () {
				$("#txac").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
			},
			success:respdf,
			error:function () {
				$("#txac").css(mal).text("Ocurrió un error");
				$("#txac").fadeIn();$("#txac").fadeOut(3000);
			}
		});
		return false;
	}
}
function respdf (res) {
	if (res=="2") {
		$("#txac").css(mal).text("Carpeta sin permisos");
		$("#txac").fadeIn();$("#txac").fadeOut(3000);
		return false;
	}
	else{
		if (res=="3") {
			$("#txac").css(mal).text("Tamaño no permitido");
			$("#txac").fadeIn();$("#txac").fadeOut(3000);
			return false;
		}
		else{
			if (res=="4") {
				$("#txac").css(mal).text("ruta no existe");
				$("#txac").fadeIn();$("#txac").fadeOut(3000);
				return false;
			}
			else{
				if (res=="5") {
					$("#txac").css(bien).text("modificado");
					$("#txac").fadeIn();$("#txac").fadeOut(3000);
					location.reload(20);
				}
				else{
					$("#txac").css(mal).html(res);
					$("#txac").fadeIn();
					return false;
				}
			}
		}
	}
}