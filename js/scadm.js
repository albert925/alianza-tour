$(document).on("ready",inicio_adminscr);
function inicio_adminscr () {
	$("#abl").on("click",abrirCjg);
	$(".cantcar").keyup(contador);
	$(".doll").on("click",recofrim);
	$(".dull").on("click",recamb);
}
function abrirCjg (e) {
	e.preventDefault();
	$("#mosG").slideToggle();
}
function contador () {
	var caracter=$(this).val();
	var cantcartot=$(this).attr("maxlength");
	var rescar=cantcartot-caracter.length;
	//console.log(rescar+"="+cantcartot+"-"+caracter.length);
	$("#numcar").text(rescar);
	if (rescar==0) {
		$("#numcar").css({color:"#CD0000"});
	}
	else{
		$("#numcar").css({color:"#008FD5"});
	}
}
function recofrim () {
	return confirm("Esta seguro de elimnarlo?");
}
function recamb (e) {
	e.preventDefault();
	var idb=$(this).attr("data-id");
	var url=$(this).attr("href");
	var rf=confirm("Esta seguro de eliminarlo?");
	if (rf==true) {
		$.post(url,{dl:idb},resultcamb);
	}
}
function resultcamb (res) {
	var ser=res.split("/");
	if (ser[0]=="2") {
		$("#ar"+ser[1]).remove();
	}
	else{
		alert(res);
	}
}