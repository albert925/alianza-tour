$(document).on("ready",inicio_promociones);
function inicio_promociones () {
	$("#nvtp").on("click",nuevo_tipo);
	$("#nvcrual").on("click",validapromoc);
	$("#nvimgpl").on("click",nuevoimagenpromo);
	$(".camtp").on("click",camb_tipo);
	$("#camarc").on("click",cambiopdf)
}
var bien={color:"#007232"};
var normal={color:"#000"};
var mal={color:"#CD0000"};
function es_imagen (tipederf) {
	switch(tipederf.toLowerCase()){
		case 'jpg':
			return true;
		case 'gif':
			return true;
		case 'png':
			return true;
		case 'jpeg':
			return true;
		default:
			return false;
	}
}
function nuevo_tipo () {
	var nm=$("#ntpl").val();
	if (nm==="") {
		$("#txA").css(mal).text("Ingrese el nombre");
		return false;
	}
	else{
		$("#txA").css(normal).text("");
		$("#txA").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' /></center>");
		$.post("new_tipo.php",{a:nm},resulti);
		return false;
	}
}
function resulti (rst) {
	if (rst=="2") {
		$("#txA").css(mal).text("Nombre tipo plan ya existe");
		return false;
	}
	else{
		if (rst=="3") {
			$("#txA").css(bien).text("Ingresado");
			location.reload(20);
		}
		else{
			$("#txA").css(mal).html(rst);
			return false;
		}
	}
}
function camb_tipo () {
	var ida=$(this).attr("data-id");
	var nmtp=$("#camtt_"+ida).val();
	$("#txb_"+ida).fadeIn();
	if (nmtp==="") {
		$("#txb_"+ida).css(mal).text("Ingrese el nombre");
	}
	else{
		$("#txb_"+ida).css(normal).text("");
		$.post("mof_tp.php",{fa:ida,a:nmtp},function (res) {
			if (res=="2") {
				$("#txb_"+ida).css(bien).text("modificado");
				$("#txb_"+ida).fadeOut(2000);
			}
			else{
				$("#txb_"+ida).css(mal).html(res);
			}
		});
	}
}
function validapromoc () {
	var sel1=$("#tpsl").val();
	if (sel1==="0" || sel1==="") {
		alert("Selecione tipo de promocion");
		$("#txA").css(mal).text("Selecione tipo de planes");
		return false;
	}
	else{
		$("#txA").css(normal).text("");
		return true;
	}
}
function nuevoimagenpromo () {
	var iga=$("#idpla").val();
	var igb=$("#igpl")[0].files[0];
	var nameigb=igb.name;
	var exteigb=nameigb.substring(nameigb.lastIndexOf('.')+1);
	if (iga=="0" || iga==="") {
		$("#jil").css(mal).text("id de plan no disponible");
		return false;
	}
	else{
		if (!es_imagen(exteigb)) {
			$("#jil").css(mal).text("tipo de imagen no permitido");
			return false;
		}
		else{
			$("#jil").css(normal).text("");
			var formu=new FormData($("#pligig")[0]);
			$.ajax({
				url: '../../../nuevoimgpromoc.php',
				type: 'POST',
				data: formu,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#jil").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
				},
				success:function (resig) {
					if (resig=="2") {
						$("#jil").css(mal).text("Carpeta sin permisos o resoluciÃ³n de imagen no permitido");
						$("#jil").fadeIn();$("#jil").fadeOut(3000);
						return false;
					}
					else{
						if (resig=="3") {
							$("#jil").css(mal).text("TamaÃ±o no permitido");
							$("#jil").fadeIn();$("#jil").fadeOut(3000);
							return false;
						}
						else{
							if (resig=="4") {
								$("#jil").css(mal).text("Carpeta sin permisos o resoluciÃ³n de imagen no permitido");
								$("#jil").fadeIn();$("#jil").fadeOut(3000);
								return false;
							}
							else{
								if (resig=="5") {
									$("#jil").css(bien).text("Imagen subida");
									$("#jil").fadeIn();$("#jil").fadeOut(3000);
									window.location.href="promo_images.php?pr="+iga;
								}
								else{
									$("#jil").css(mal).html(resig);
									$("#jil").fadeIn();
									return false;
								}
							}
						}
					}
				},
				error:function () {
					$("#jil").css(mal).text("OcurriÃ³ un error");
					$("#jil").fadeIn();$("#jil").fadeOut(3000);
				}
			});
			return false;
		}
	}
}
function cambiopdf () {
	var idg=$("#idp").val()
	var pdf=$("#pfp")[0].files[0];
	var namepdf=pdf.name;
	var extepdf=namepdf.substring(namepdf.lastIndexOf('.')+1);
	var tampdf=pdf.size;
	var tipopdf=pdf.type;
	if (idg=="" || idg=="0") {
		$("#txac").css(mal).text("id de plan no disponible");
		return false;
	}
	else{
		$("#txac").css(normal).text("");
		var forig=new FormData($("#arch")[0]);
		$.ajax({
			url: '../../../mofarchipromo.php',
			type: 'POST',
			data: forig,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend:function () {
				$("#txac").prepend("<center><img src='../../../imagenes/loadingb.gif' alt='loading' style='width:20px;' /></center>");
			},
			success:respdf,
			error:function () {
				$("#txac").css(mal).text("Ocurrió un error");
				$("#txac").fadeIn();$("#txac").fadeOut(3000);
			}
		});
		return false;
	}
}
function respdf (res) {
	if (res=="2") {
		$("#txac").css(mal).text("Carpeta sin permisos");
		$("#txac").fadeIn();$("#txac").fadeOut(3000);
		return false;
	}
	else{
		if (res=="3") {
			$("#txac").css(mal).text("Tamaño no permitido");
			$("#txac").fadeIn();$("#txac").fadeOut(3000);
			return false;
		}
		else{
			if (res=="4") {
				$("#txac").css(mal).text("ruta no existe");
				$("#txac").fadeIn();$("#txac").fadeOut(3000);
				return false;
			}
			else{
				if (res=="5") {
					$("#txac").css(bien).text("modificado");
					$("#txac").fadeIn();$("#txac").fadeOut(3000);
					location.reload(20);
				}
				else{
					$("#txac").css(mal).html(res);
					$("#txac").fadeIn();
					return false;
				}
			}
		}
	}
}