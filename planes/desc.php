<?php
	include '../config.php';
	$idR=$_GET['pl'];
	if ($idR=="") {
		echo "<script type='text/javascript'>";
			echo "alert('id de plan no disponible');";
			echo "var pagina='../planes';";
			echo "document.location.href=pagina;";
		echo "</script>";
	}
	else{
		$datos="SELECT * from planes inner join tip_planes on(planes.tp_pl=tip_planes.id_tppl) 
			where id_pl=$idR";
		$sql_datos=mysql_query($datos,$conexion) or die (mysql_error());
		$numdat=mysql_num_rows($sql_datos);
		if ($numdat>0) {
			while ($dt=mysql_fetch_array($sql_datos)) {
				$idpl=$dt['id_pl'];
				$tppl=$dt['tp_pl'];
				$nmpl=$dt['tit_pl'];
				$sbpl=$dt['sub_pl'];
				$xxpl=$dt['txt_pl'];
				$espl=$dt['es_pl'];
				$fepl=$dt['fe_pl'];
				$nmug=$dt['nm_tppl'];
				$rtug=$dt['rut_tppl'];
			}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, maximun-scale=1" />
	<meta name="description" content="planes <?php echo $nmpl ?>" />
	<meta name="keywords" content="ver decripción del plan" />
	<title><?php echo "$nmpl"; ?> | Alianza Tour</title>
	<link rel="icon" href="../imagenes/icono.png" />
	<link rel="image_src" href="../imagenes/logo.png" />
	<link rel="stylesheet" href="../css/normalize.css" />
	<link rel="stylesheet" href="../css/iconos/style.css" />
	<link rel="stylesheet" href="../css/default/default.css" />
	<link rel="stylesheet" href="../css/nivo_slider.css" />
	<link rel="stylesheet" href="../css/owl_carousel.css" />
	<link rel="stylesheet" href="../css/owl_theme_min.css" />
	<link rel="stylesheet" href="../css/style.css" />
	<link rel="stylesheet" href="../css/inicio.css" />
	<link rel="stylesheet" href="../css/shadowbox.css" />
	<script src="../js/jquery_2_1_1.js"></script>
	<script src="../js/owl_carousel_min.js"></script>
	<script src="../js/scripag.js"></script>
</head>
<body>
	<figure id="inic">
		<section id="dosimg" class="divider-wrapper">
			<div class="code-wrapper">
				<img src="../imagenes/image-a.jpg" alt="home" />
				<div class="design-wrapper">
					<div class="design-image">
						<img src="../imagenes/image-b.jpg" alt="home" />
					</div>
				</div>
			</div>
			<div class="divider-bar"></div>
		</section>
	</figure>
	<header>
		<figure id="logo">
			<a href="../">
				<img src="../imagenes/logob.png" alt="logo" />
			</a>
		</figure>
		<div id="mn_mv"><span class="icon-menu"></span></div>
		<nav id="mnP">
			<ul>
				<li><a href="../">Inicio</a></li>
				<li><a href="../nosotros">Nosotros</a></li>
				<li class="submen" data-num="1">
					<a class="sull" href="../planes">Planes</a>
					<ul class="children1">
						<?php
							$tpPL="SELECT * from tip_planes order by id_tppl desc";
							$sql_pl=mysql_query($tpPL,$conexion) or die (mysql_error());
							while ($pl=mysql_fetch_array($sql_pl)) {
								$idlp=$pl['id_tppl'];
								$nmlp=$pl['nm_tppl'];
						?>
						<li><a href="../planes/tipos.php?tp=<?php echo $idlp ?>"><?php echo "$nmlp"; ?></a></li>
						<?php
							}
						?>
					</ul>
				</li>
				<li class="submen" data-num="2"><a href="../promociones">Promociones</a>
					<ul class="children2">
						<?php
							$tpPR="SELECT * from tip_pr order by id_tpr desc";
							$sql_pr=mysql_query($tpPR,$conexion) or die (mysql_error());
							while ($pr=mysql_fetch_array($sql_pr)) {
								$idpr=$pr['id_tpr'];
								$nmpr=$pr['nm_tpr'];
						?>
						<li><a href="../promociones/tipos.php?tp=<?php echo $idpr ?>"><?php echo "$nmpr"; ?></a></li>
						<?php
							}
						?>
					</ul>
				</li>
				<li><a href="../documento">Documentos</a></li>
				<li><a href="../contacto">Contacto</a></li>
			</ul>
		</nav>
		<div id="usuario">
			<span class="icon-login"></span>
		</div>
	</header>
	<section>
		<figure class="otrosS">
			<div class="slider-wrapper theme-default">
				<div id="sliderBB" class="nivoSlider">
					<?php
						$redils="SELECT * from slider_tx order by id_slix desc";
						$sql_ils=mysql_query($redils,$conexion) or die (mysql_error());
						while ($is=mysql_fetch_array($sql_ils)) {
							$idsl=$is['id_slix'];
							$ttsl=$is['tt_sli'];
							$sbsl=$is['sb_sli'];
							$resl=$is['re_sli'];
							$lksl=$is['lk_sli'];
							$rtsl=$is['rut_sli'];
					?>
					<img src="../<?php echo $rtsl ?>" alt="<?php echo $ttsl ?>" />
					<?php
						}
					?>
				</div>
			</div>
		</figure>
		<h1 id="hdos"><?php echo "$nmug"; ?></h1>
		<article id="automargen" class="descG">
			<h2 class="glyh"><?php echo "$nmpl"; ?></h2>
			<figure id="galery">
				<div class="slider-wrapper theme-default">
					<div id="slider" class="nivoSlider">
						<?php
							$cucru="SELECT * from images_planes where pl_id=$idR order by id_img_pl asc";
							$sql_cru=mysql_query($cucru,$conexion) or die (mysql_error());
							while ($tr=mysql_fetch_array($sql_cru)) {
								$idmg=$tr['id_img_pl'];
								$rtmg=$tr['rut_pl'];
						?>
						<a href="../<?php echo $rtmg ?>" rel="shadowbox[Vacation]">
							<img src="../<?php echo $rtmg ?>" alt="<?php echo $idmg ?>" />
						</a>
						<?php
							}
						?>
					</div>
				</div>
			</figure>
			<article class="textg">
				<?php echo "$xxpl"; ?>
			</article>
		</article>
		<article class="alianzz">
			<article id="automargen">
				<article class="owl-carousel-b owl-theme owl-loaded">
					<?php
						$alz="SELECT * from proveedores order by id_pv desc";
						$sql_alz=mysql_query($alz,$conexion) or die (mysql_error());
						while ($az=mysql_fetch_array($sql_alz)) {
							$idpv=$az['id_pv'];
							$ttpv=$az['tt_pv'];
							$lkpv=$az['lk_pv'];
							$rtpv=$az['rut_pv'];
					?>
					<div class="item">
						<figure>
							<a href="<?php echo $lkpv ?>" target="_blank">
								<img src="../<?php echo $rtpv ?>" alt="<?php echo $ttpv ?>" />
							</a>
						</figure>
					</div>
					<?php
						}
					?>
				</article>
			</article>
		</article>
	</section>
	<section>
		<article id="map_canvas" class="mapas"></article>
	</section>
	<footer>
		<article class="foorflx">
			<article class="ar arr2">
				<h2>Alianza Tour </h2>
				<p>
					Brindar el mejor servicio de asesoría y venta de paquetes turísticos a 
					nivel local, nacional e internacional, con el fin de lograr la plena 
					satisfacción del cliente, brindando servicios que superen sus expectativas 
					mediante un servicio confiable, accesible y respaldado por un grupo de 
					trabajo profesional, incentivado por el buen trato y el impulso al 
					desarrollo individual de cada uno de los integrantes nuestra organización.
				</p>
			</article>
			<article class="ar">
				<h2>Contáctanos</h2>
				<div>Av 9 #5-53 Barrio panamericano</div>
				<div>Cúcuta Colombia</div>
				<div>Whatsapp: 311 806 8590</div>
				<div>Tel: 572 5613 - 572 4592 - 317 376 4207</div>
				<div>Email: ventas@alianzatourcucuta.com</div>
			</article>
			<article class="ar">
				<article id="redes">
					<a href="https://www.facebook.com/alianzatourcucuta/?fref=ts" target="_blank"><span class="icon-facebook4"></span></a>
					<a href="https://www.instagram.com/alianzatour/" target="_blank"><span class="icon-instagram2"></span></a>
				</article>
			</article>
		</article>
		<article class="footfin">
			CONAXPORT © 2015 &nbsp;&nbsp;todos los derechos reservados &nbsp;- &nbsp;PBX (5) 841 733 &nbsp;&nbsp;Cúcuta - Colombia &nbsp;&nbsp;
			<a href="http://conaxport.com/" target="_blank">www.conaxport.com</a>
		</article>
	</footer>
	<script src="../js/nivo_slider.js"></script>
	<script src="../js/sliimages.js"></script>
	<script src="../js/sliddiv.js"></script>
	<script src="../js/shadowbox.js"></script>
	<script type="text/javascript">
		Shadowbox.init({
			handleOversize:"drag",
			modal:true
		});
	</script>
	<script src="../js/init.js"></script>
	<script src="../js/nivo_slider.js"></script>
	<script src="../js/sliimages.js"></script>
	<script src="http://www.google.com/jsapi"></script>
	<script src="../js/colmapa.js"></script>
</body>
</html>
<?php
			}
		else{
			echo "<script type='text/javascript'>";
				echo "alert('plan no existe o ha sido eliminado');";
				echo "var pagina='../planes';";
				echo "document.location.href=pagina;";
			echo "</script>";
		}
	}
?>