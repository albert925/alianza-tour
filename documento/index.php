<?php
	include '../config.php';
	$idR=$_GET['dc'];
	if ($idR=="") {
		$docu="SELECT * from documento order by id_dc desc limit 1";
		$sql_docu=mysql_query($docu,$conexion) or die (mysql_error());
		while ($dc=mysql_fetch_array($sql_docu)) {
			$idc=$dc['id_dc'];
			$mndc=$dc['tt_mn'];
			$ttdc=$dc['tt_dc'];
			$xxdc=$dc['txt_dc'];
			$fedc=$dc['fe_dc'];
		}	
	}
	else{
		$docu="SELECT * from documento where id_dc=$idR";
		$sql_docu=mysql_query($docu,$conexion) or die (mysql_error());
		while ($dc=mysql_fetch_array($sql_docu)) {
			$idc=$dc['id_dc'];
			$mndc=$dc['tt_mn'];
			$ttdc=$dc['tt_dc'];
			$xxdc=$dc['txt_dc'];
			$fedc=$dc['fe_dc'];
		}	
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, maximun-scale=1" />
	<meta name="description" content="archivos y documentos" />
	<meta name="keywords" content="TOdos los documentos o archivos" />
	<title><?php echo "$mndc"; ?> | Alianza Tour</title>
	<link rel="icon" href="../imagenes/icono.png" />
	<link rel="image_src" href="../imagenes/logo.png" />
	<link rel="stylesheet" href="../css/normalize.css" />
	<link rel="stylesheet" href="../css/iconos/style.css" />
	<link rel="stylesheet" href="../css/default/default.css" />
	<link rel="stylesheet" href="../css/nivo_slider.css" />
	<link rel="stylesheet" href="../css/owl_carousel.css" />
	<link rel="stylesheet" href="../css/owl_theme_min.css" />
	<link rel="stylesheet" href="../css/style.css" />
	<link rel="stylesheet" href="../css/inicio.css" />
	<script src="../js/jquery_2_1_1.js"></script>
	<script src="../js/owl_carousel_min.js"></script>
	<script src="../js/scripag.js"></script>
	<script src="../js/archivosP.js"></script>
</head>
<body>
	<figure id="inic">
		<section id="dosimg" class="divider-wrapper">
			<div class="code-wrapper">
				<img src="../imagenes/image-a.jpg" alt="home" />
				<div class="design-wrapper">
					<div class="design-image">
						<img src="../imagenes/image-b.jpg" alt="home" />
					</div>
				</div>
			</div>
			<div class="divider-bar"></div>
		</section>
	</figure>
	<header>
		<figure id="logo">
			<a href="../">
				<img src="../imagenes/logob.png" alt="logo" />
			</a>
		</figure>
		<div id="mn_mv"><span class="icon-menu"></span></div>
		<nav id="mnP">
			<ul>
				<li><a href="../">Inicio</a></li>
				<li><a href="../nosotros">Nosotros</a></li>
				<li class="submen" data-num="1">
					<a href="../planes">Planes</a>
					<ul class="children1">
						<?php
							$tpPL="SELECT * from tip_planes order by id_tppl desc";
							$sql_pl=mysql_query($tpPL,$conexion) or die (mysql_error());
							while ($pl=mysql_fetch_array($sql_pl)) {
								$idlp=$pl['id_tppl'];
								$nmlp=$pl['nm_tppl'];
						?>
						<li><a href="../planes/tipos.php?tp=<?php echo $idlp ?>"><?php echo "$nmlp"; ?></a></li>
						<?php
							}
						?>
					</ul>
				</li>
				<li class="submen" data-num="2"><a href="../promociones">Promociones</a>
					<ul class="children2">
						<?php
							$tpPR="SELECT * from tip_pr order by id_tpr desc";
							$sql_pr=mysql_query($tpPR,$conexion) or die (mysql_error());
							while ($pr=mysql_fetch_array($sql_pr)) {
								$idpr=$pr['id_tpr'];
								$nmpr=$pr['nm_tpr'];
						?>
						<li><a href="../promociones/tipos.php?tp=<?php echo $idpr ?>"><?php echo "$nmpr"; ?></a></li>
						<?php
							}
						?>
					</ul>
				</li>
				<li><a class="sull" href="../documento">Documentos</a></li>
				<li><a href="../contacto">Contacto</a></li>
			</ul>
		</nav>
		<div id="usuario">
			<span class="icon-login"></span>
		</div>
	</header>
	<section>
		<figure class="otrosS">
			<div class="slider-wrapper theme-default">
				<div id="sliderBB" class="nivoSlider">
					<?php
						$redils="SELECT * from slider_tx order by id_slix desc";
						$sql_ils=mysql_query($redils,$conexion) or die (mysql_error());
						while ($is=mysql_fetch_array($sql_ils)) {
							$idsl=$is['id_slix'];
							$ttsl=$is['tt_sli'];
							$sbsl=$is['sb_sli'];
							$resl=$is['re_sli'];
							$lksl=$is['lk_sli'];
							$rtsl=$is['rut_sli'];
					?>
					<img src="../<?php echo $rtsl ?>" alt="<?php echo $ttsl ?>" />
					<?php
						}
					?>
				</div>
			</div>
		</figure>
		<article id="automargen" class="flxmnv">
			<nav class="mnVv">
				<?php
					$Tdodc="SELECT * from documento order by id_dc asc";
					$sql_docg=mysql_query($Tdodc,$conexion) or die (mysql_error());
					while ($gcd=mysql_fetch_array($sql_docg)) {
						$cdid=$gcd['id_dc'];
						$cdtt=$gcd['tt_mn'];
						if ($cdid==$idc) {
							$selmenu="class='selvv'";
						}
						else{
							$selmenu="";
						}
				?>
				<a <?php echo $selmenu ?> href="index.php?dc=<?php echo $cdid ?>"><?php echo "$cdtt"; ?></a>
				<?php
					}
				?>
			</nav>
			<section>
				<article class="PLN">
					<h2><?php echo "$ttdc"; ?></h2>
					<article>
						<?php echo "$xxdc"; ?>
					</article>
				</article>
				<article class="arch tpar">
					<?php
						error_reporting(E_ALL ^ E_NOTICE);
						$tamno_pagina=15;
						$pagina= $_GET['pagina'];
						if (!$pagina) {
							$inicio=0;
							$pagina=1;
						}
						else{
							$inicio= ($pagina - 1)*$tamno_pagina;
						}
						$ssql="SELECT * from tp_ar where doc_id=$idc order by id_tpar desc";
						$rs=mysql_query($ssql,$conexion) or die (mysql_error());
						$num_total_registros= mysql_num_rows($rs);
						$total_paginas= ceil($num_total_registros / $tamno_pagina);
						$gsql="SELECT * from tp_ar where doc_id=$idc order by id_tpar desc limit $inicio, $tamno_pagina";
						$impsql=mysql_query($gsql,$conexion) or die (mysql_error());
						while ($gh=mysql_fetch_array($impsql)) {
							$idtar=$gh['id_tpar'];
							$tptar=$gh['nm_tpar'];
					?>
					<article class="ararc" data-id="<?php echo $idtar ?>">
						<h2 data-id="<?php echo $idtar ?>"><?php echo "$tptar"; ?></h2>
						<div id="v<?php echo $idtar ?>" class="verarch">
						</div>
					</article>
					<?php
						}
					?>
				</article>
				<article>
					<br />
					<b>Páginas: </b>
					<?php
						//muestro los distintos indices de las paginas
						if ($total_paginas>1) {
							for ($i=1; $i <=$total_paginas ; $i++) { 
								if ($pagina==$i) {
									//si muestro el indice del la pagina actual, no coloco enlace
						?>
							<b><?php echo $pagina." "; ?></b>
						<?php
								}
								else{
									//si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página 
						?>
									<a href="tipos.php?pagina=<?php echo $i ?>"><?php echo "$i"; ?></a>

						<?php
								}
							}
						}
					?>
				</article>
			</section>
		</article>
		<article class="alianzz">
			<article id="automargen">
				<article class="owl-carousel-b owl-theme owl-loaded">
					<?php
						$alz="SELECT * from proveedores order by id_pv desc";
						$sql_alz=mysql_query($alz,$conexion) or die (mysql_error());
						while ($az=mysql_fetch_array($sql_alz)) {
							$idpv=$az['id_pv'];
							$ttpv=$az['tt_pv'];
							$lkpv=$az['lk_pv'];
							$rtpv=$az['rut_pv'];
					?>
					<div class="item">
						<figure>
							<a href="<?php echo $lkpv ?>" target="_blank">
								<img src="../<?php echo $rtpv ?>" alt="<?php echo $ttpv ?>" />
							</a>
						</figure>
					</div>
					<?php
						}
					?>
				</article>
			</article>
		</article>
	</section>
	<section>
		<article id="map_canvas" class="mapas"></article>
	</section>
	<footer>
		<article class="foorflx">
			<article class="ar arr2">
				<h2>Alianza Tour </h2>
				<p>
					Brindar el mejor servicio de asesoría y venta de paquetes turísticos a 
					nivel local, nacional e internacional, con el fin de lograr la plena 
					satisfacción del cliente, brindando servicios que superen sus expectativas 
					mediante un servicio confiable, accesible y respaldado por un grupo de 
					trabajo profesional, incentivado por el buen trato y el impulso al 
					desarrollo individual de cada uno de los integrantes nuestra organización.
				</p>
			</article>
			<article class="ar">
				<h2>Contáctanos</h2>
				<div>Av 9 #5-53 Barrio panamericano</div>
				<div>Cúcuta Colombia</div>
				<div>Whatsapp: 311 806 8590</div>
				<div>Tel: 572 5613 - 572 4592 - 317 376 4207</div>
				<div>Email: ventas@alianzatourcucuta.com</div>
			</article>
			<article class="ar">
				<article id="redes">
					<a href="https://www.facebook.com/alianzatourcucuta/?fref=ts" target="_blank"><span class="icon-facebook4"></span></a>
					<a href="https://www.instagram.com/alianzatour/" target="_blank"><span class="icon-instagram2"></span></a>
				</article>
			</article>
		</article>
		<article class="footfin">
			CONAXPORT © 2015 &nbsp;&nbsp;todos los derechos reservados &nbsp;- &nbsp;PBX (5) 841 733 &nbsp;&nbsp;Cúcuta - Colombia &nbsp;&nbsp;
			<a href="http://conaxport.com/" target="_blank">www.conaxport.com</a>
		</article>
	</footer>
	<script src="../js/sliddiv.js"></script>
	<script src="../js/init.js"></script>
	<script src="../js/nivo_slider.js"></script>
	<script src="../js/sliimages.js"></script>
	<script src="http://www.google.com/jsapi"></script>
	<script src="../js/colmapa.js"></script>
</body>
</html>